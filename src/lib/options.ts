export interface IOptions {
    divisorOne: number,
    divisorOnePhrase: string,
    divisorTwo: number,
    divisorTwoPhrase: string
}