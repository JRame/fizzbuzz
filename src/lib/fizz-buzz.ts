import { IOptions } from "./options";
import { FizzBuzzDefaultOptions } from "@/lib/fizz-buzz-default-options";

export class FizzBuzz {
	private divisorOne: number;
	private divisorOnePhrase: string;
	private divisorTwo: number;
	private divisorTwoPhrase: string;

	constructor(options: IOptions) {
		this.divisorOne = options.divisorOne || FizzBuzzDefaultOptions.divisorOne
		this.divisorOnePhrase = options.divisorOnePhrase || FizzBuzzDefaultOptions.divisorOnePhrase; 
		this.divisorTwo = options.divisorTwo || FizzBuzzDefaultOptions.divisorTwo;
		this.divisorTwoPhrase = options.divisorTwoPhrase || FizzBuzzDefaultOptions.divisorTwoPhrase;
	}

    public translate(value: number): string {
		if (isNaN(value) || value === 0) {
			throw new Error('Enter a Number');
		}

		if (value % this.divisorOne == 0 && value % this.divisorTwo == 0)
			return `${this.divisorOnePhrase}${this.divisorTwoPhrase}`;
		else if (value % this.divisorTwo == 0)
			return this.divisorTwoPhrase;
		else if (value %  this.divisorOne == 0)
			return this.divisorOnePhrase;

		return value.toString();
   }
}