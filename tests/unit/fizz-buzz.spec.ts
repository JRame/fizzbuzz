import { FizzBuzz } from '@/lib/fizz-buzz'

describe('fizzbuzz', () => {
  const defaultOptions = {
    divisorOne: 3,
    divisorOnePhrase: 'Fizz',
    divisorTwo: 5,
    divisorTwoPhrase: 'Buzz'
  };

  it('return 1', () => {
    const fizzbuzz = new FizzBuzz(defaultOptions);
  
    expect(fizzbuzz.translate(1)).toMatch('1');
  });

  it('return 2', () => {
    const fizzbuzz = new FizzBuzz(defaultOptions);
  
    expect(fizzbuzz.translate(2)).toMatch('2');
  });

  it('return fizz for 3', () => {
    const fizzbuzz = new FizzBuzz(defaultOptions);

    expect(fizzbuzz.translate(3)).toMatch('Fizz');
  });

  it('return phrase one for 3', () => {
    const fizzbuzz = new FizzBuzz({
      ...defaultOptions,
      divisorOnePhrase: 'MyPhrase'
    });
  
    expect(fizzbuzz.translate(3)).toMatch('MyPhrase');
  });

  it('return 4', () => {
    const fizzbuzz = new FizzBuzz(defaultOptions);

    expect(fizzbuzz.translate(4)).toMatch('4');
  });

  it('return buzz for 5', () => {
    const fizzbuzz = new FizzBuzz(defaultOptions);

    expect(fizzbuzz.translate(5)).toMatch('Buzz');
  });

  it('return phrase two for 5', () => {
    const fizzbuzz = new FizzBuzz({
      ...defaultOptions,
      divisorTwoPhrase: 'MyPhrase'
    });

    expect(fizzbuzz.translate(5)).toMatch('MyPhrase');
  });

  it('return fizz for 9', () => {
    const fizzbuzz = new FizzBuzz(defaultOptions);

    expect(fizzbuzz.translate(9)).toMatch('Fizz');
  });

  it('return buzz for 10', () => {
    const fizzbuzz = new FizzBuzz(defaultOptions);

    expect(fizzbuzz.translate(10)).toMatch('Buzz');
  });

  it('return fizzbuzz for 15', () => {
    const fizzbuzz = new FizzBuzz(defaultOptions);

    expect(fizzbuzz.translate(15)).toMatch('FizzBuzz');
  });

  it('return phrases for 15', () => {
    const fizzbuzz = new FizzBuzz({
      ...defaultOptions,
      divisorOnePhrase: 'My',
      divisorTwoPhrase: 'Phrases'
    });
  
    expect(fizzbuzz.translate(15)).toMatch('MyPhrases');
  });

  it('uses new divisor one', () => {
    const fizzbuzz = new FizzBuzz({
      ...defaultOptions,
      divisorOne: 6
    });
  
    expect(fizzbuzz.translate(6)).toMatch('Fizz');
  });

  it('uses new divisor two', () => {
    const fizzbuzz = new FizzBuzz({
      ...defaultOptions,
      divisorTwo: 7
    });
  
    expect(fizzbuzz.translate(14)).toMatch('Buzz');
  });

  it('return empty for invalid number', () => {
    const fizzbuzz = new FizzBuzz(defaultOptions);
  
    expect(fizzbuzz.translate(NaN)).toMatch('Enter a Number');
  });

  it('return empty for 0', () => {
    const fizzbuzz = new FizzBuzz(defaultOptions);
  
    expect(fizzbuzz.translate(0)).toMatch('Enter a Number');
  });
})